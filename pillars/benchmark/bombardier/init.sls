# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Bombardier pillar

{% from "common.yaml" import common with context %}

bombardier:
  enable: {{ common.hostgroup == 'client' }}
