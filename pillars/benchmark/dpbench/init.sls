# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Dpbench pillar

{% from "common.yaml" import common with context %}

dpbench:
  enable: true
  hserver: {{ common.hostgroup == 'backend' }}
  hserver_port: 80
