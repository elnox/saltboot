# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Iptables pillar

{% from "common.yaml" import common with context %}

iptables:

  # Config
  enable: true
  strict: true

  # Whitelist rules
  whitelist:
    ipv4: {}

  # Table rules
  filter:
    INPUT:
      rules:
        log-denied:
          match: limit
          limit: "5/min"
          jump: LOG
          log-prefix: "iptables denied: "
          log-level: 7
    FORWARD:
      policy: DROP

  # Services rules
  services:
    ipv4:
      '22':
        comment: "Allow ssh access"
        ips_allow: [ '0.0.0.0/0' ]
      '80':
        comment: "Allow http access"
        ips_allow: [ '10.0.0.0/8' ]
      '25':
        comment: "Allow smtp access"
        ips_allow: [ '10.0.0.0/8' ]
      '53':
        comment: "Allow named access"
        ips_allow: [ '10.0.0.0/8' ]
        protos: ['tcp', 'udp']
