# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Sysctl pillar

{%- from "common.yaml" import common with context %}

{% set mem_90 = (90 * grains['mem_total'] * 1024 / 100) %}

sysctl:
  settings:
    fs.file-max: 10000000
    net.core.somaxconn: 10000000                # default: 128
    net.core.netdev_max_backlog: 10000000       # default: 1000
    net.core.optmem_max: 16777216               # default: 20480
    net.core.rmem_default: 16777216             # default: 212992
    net.core.rmem_max: {{ mem_90 | int }}       # default: 212992 13421772
    net.core.wmem_default: 16777216             # default: 212992
    net.core.wmem_max: {{ mem_90 | int }}       # default: 212992 13421772
    net.ipv4.tcp_mem: 8388608 8388608 83886080  # default: 2298528 3064704 4597056
    net.ipv4.tcp_rmem: 1048576 4194304 16777216 # default: 4096 87380 6291456
    net.ipv4.tcp_wmem: 1048576 4194304 16777216 # default: 4096 16384 4194304
    net.ipv4.tcp_keepalive_intvl: 15            # default: 75
    net.ipv4.tcp_keepalive_probes: 5            # default: 9
    net.ipv4.tcp_keepalive_time: 30             # default: 7200
    net.ipv4.tcp_sack: 1                        # default: 1
    net.ipv4.tcp_syn_retries: 5                 # default: 6
    net.ipv4.tcp_synack_retries: 3              # default: 5
    net.ipv4.tcp_fin_timeout: 10                # default: 60
    net.ipv4.tcp_slow_start_after_idle: 0       # default: 1
    net.ipv4.tcp_mtu_probing: 1                 # default: 0
    net.ipv4.tcp_low_latency: 1                 # default: 0
    net.ipv4.tcp_moderate_rcvbuf: 1             # default: 1
    net.ipv4.tcp_tw_reuse: 1                    # default: 0
    net.ipv4.tcp_window_scaling: 1              # default: 1
    net.ipv4.tcp_max_syn_backlog: 10000000      # default: 2048
    net.ipv4.ip_local_port_range: 1024 65535    # default: 32768 60999
