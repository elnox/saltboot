# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Timezone pillar

timezone:
  name: 'Europe/Paris'
  utc:  False
