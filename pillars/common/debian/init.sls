# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Debian pillar

{%- from "common.yaml" import common with context %}

debian:

  sudoers: true

  journald:
    MaxLevelStore:      info
    MaxLevelSyslog:     info
    MaxLevelConsole:    debug

  services:
    - salt-minion

  limits:
    nproc:
      soft:
        "*": {{ 32768 }}
      hard:
        "*": {{ 32768 }}
    nofile:
      soft:
        "*": {{ 32768 }}
      hard:
        "*": {{ 32768 }}

  files: {}

  directories:
    srv:
      path: /srv
      user: root
      group: root
    scripts:
      path: /scripts
      user: root
      group: root

  symlinks:
    - /srv/salt/utils/bench

  remove:
    - /var/log/auth.log
    - /var/log/cron.log
    - /var/log/daemon.log
    - /var/log/fontconfig.log
    - /var/log/kern.log
    - /var/log/user.log
    - /var/log/debug
    - /var/log/error
    - /var/log/installer

  packages:
    {%- for state in [ 'present', 'purged' ] %}
    {{ state }}:
      {%- for pkgs, data in common.packages | dictsort %}
      {%-   if data.groups is defined and common.group in data.groups %}
      {%-     for pkg in data.get(state, []) %}
      - {{ pkg }}
      {%-     endfor %}
      {%-   elif data.groups is not defined %}
      {%-     if data.exclude is defined and common.host not in data.exclude %}
      {%-       for pkg in data.get(state, []) %}
      - {{ pkg }}
      {%-       endfor %}
      {%-     elif data.exclude is not defined %}
      {%-       for pkg in data.get(state, []) %}
      - {{ pkg }}
      {%-       endfor %}
      {%-     endif %}
      {%-   endif %}
      {%- endfor %}
    {%- endfor %}
