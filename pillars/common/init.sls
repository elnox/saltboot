# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Common stuff pillar

# wanted everywhere
{% set mandatories  = [ 'apt', 'bash', 'debian', 'hostname', 'locale', 'logrotate', 'repos', 'sysctl', 'syslog', 'timezone', 'vim' ] %}

# compute a list of pillars we want to include
{%- set enabled = [] %}
{%- for component in mandatories %}
{%-   do enabled.append(slspath + "." + component) %}
{%- endfor %}

# include wanted pillars
include: {{ enabled }}
