# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Git repository pillar

{%- from "common.yaml" import common with context %}

repos:

  deploy:
    salt:
      url:      https://gitlab.com/elnox/saltboot.git
      target:   '/srv/salt'
      rev:      'main'
      branch:   'main'
      reset:    true
