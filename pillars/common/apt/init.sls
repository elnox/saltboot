# -*- coding: utf-8 -*-
# vim: ft=yaml
#
# Apt pillar

{%- from "common.yaml" import common with context %}

# define data
{%- set mirror = 'http://' ~ common.os ~ '.mirrors.ovh.net/' ~ common.os ~ '/' %}
{%- if common.os == 'Debian' %}
{%-   set repos_comps = [ 'main', 'contrib' ] %}
{%-   set security_url = 'http://security.debian.org/debian-security' %}
{%-   set security_distro = common.distro ~ '/updates' if common.distro in [ 'buster' ] else common.distro ~ '-security' %}
{%- elif common.os == 'Ubuntu' %}
{%-   set repos_comps = [ 'main', 'restricted', 'universe', 'multiverse' ] %}
{%-   set security_url = 'http://security.ubuntu.com/ubuntu' %}
{%-   set security_distro = common.distro ~ '-security' %}
{%- endif %}

apt:

  # default
  default_url:          {{ mirror | lower }}
  remove_preferences:   {{ not common.bootstrap }}
  remove_sources_list:  {{ not common.bootstrap }}
  sources_list_clean:   {{ not common.bootstrap }}
  preferences_clean:    {{ not common.bootstrap }}

  # globals setup
  confd:
    01norecommend:
      'APT::Install-Recommends': "0"
      'APT::Install-Suggests': "0"
    30release:
      'APT::Default-Release': {{ common.distro }}
    40dpkg-options:
      'Dpkg::Options':
        - '--force-confdef'
        - '--force-confold'
    99-force-ipv4:
      'Acquire::ForceIPv4': 'true'

  listchanges:
    profiles:
      apt:
        frontend:       pager
        email_address:  root
        save_seen:      /var/lib/apt/listchanges.db
        which:          news
        email_format:   text
        confirm:        false
        headers:        false
        reverse:        false

  preferences:
    stable.pref:
      pin:              "release a=stable"
      priority:         900

  unattended:
    allowed_origins:
      - '${distro_id}:${distro_codename}'
      - '${distro_id}:${distro_codename}-security'
      - '${distro_id}ESMApps:${distro_codename}-apps-security'
      - '${distro_id}ESM:${distro_codename}-infra-security'

  # repositories setup
  repositories:
    {{ common.os | lower }}-{{ common.distro }}:
      distro:     {{ common.distro }}
      url:        {{ mirror | lower }}
      comps:      {{ repos_comps }}
      type:       [ binary ]
    security-{{ common.distro }}:
      distro:     {{ security_distro | lower }}
      url:        {{ security_url }}
      comps:      {{ repos_comps }}
      type:       [ binary ]
    updates-{{ common.distro }}:
      distro:     {{ common.distro ~ "-updates" | lower }}
      url:        {{ mirror | lower }}
      comps:      {{ repos_comps }}
    backports-{{ common.distro  }}:
      distro:     {{ common.distro ~ "-backports" | lower }}
      url:        {{ mirror | lower }}
      comps:      {{ repos_comps }}
