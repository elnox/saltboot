# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Locale pillar

locale:
  present:
    - "en_US ISO-8859-1"
    - "en_US.UTF-8"
  default:
    name:     "en_US.UTF-8"
    require:  "en_US.UTF-8"
