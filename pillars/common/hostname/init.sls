# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Hostname pillar

{%- from "common.yaml" import common with context %}

hostname:
  fqdn: {{ common.fqdn }}
  name: {{ common.host }}
  manage_hosts: true
  ips: {{ grains['ipv4'] + ['127.0.1.1'] + grains['ipv6'] }}
