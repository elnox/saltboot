# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Haproxy pillar

{% from "common.yaml" import common with context %}

{%- set num_cpus = 64 if grains['num_cpus'] > 64 else grains['num_cpus'] %}

haproxy:
  enable: {{ common.hostgroup == 'backend' }}

  scripts:
    200.lua: /etc/haproxy

  global:
    nbproc: 1
    nbthread: {{ num_cpus }}
    maxconn: {{ 50000 }}
    ulimit: 6400585
    cpu-map:
      - auto:1/1-{{ num_cpus }} 0-{{ num_cpus - 1 }}
    log:
      - 127.0.0.1 local2 debug
    lua-load:
      - /etc/haproxy/200.lua

    stats:
      timeout: 30s
      socketpath: /run/haproxy/admin.sock
      mode: 660
      level: admin
      extra: expose-fd listeners

  defaults:
    mode: tcp
    option:
      - tcplog
      - dontlognull
      - redispatch
      - abortonclose
    log: global
    timeout:
      - connect         5s
      - server          1m
      - client          1m
    maxconn: {{ 50000 }}
    fullconn: {{ 50000 }}
    errorfiles:
      {% for code in [ 400, 403, 408, 500, 502, 504 ] %}
      {{ code }}: /etc/haproxy/errors/{{ code }}.http
      {% endfor %}

  listens:
    http:
      bind:
        - "*:8080"
      mode: http
      acl:
        - "0k path_beg /0k"
        - "1k path_beg /1k"
        - "2k path_beg /2k"
        - "3k path_beg /3k"
        - "4k path_beg /4k"
        - "5k path_beg /5k"
        - "6k path_beg /6k"
        - "12k path_beg /12k"
        - "24k path_beg /24k"
        - "48k path_beg /48k"
      http-request:
        - "use-service lua.0k if 0k"
        - "use-service lua.1k if 1k"
        - "use-service lua.2k if 2k"
        - "use-service lua.3k if 3k"
        - "use-service lua.4k if 4k"
        - "use-service lua.5k if 5k"
        - "use-service lua.6k if 6k"
        - "use-service lua.12k if 12k"
        - "use-service lua.24k if 24k"
        - "use-service lua.48k if 48k"
        - "use-service lua.ok"
      http-response:
        - "set-header X-Custom {{ grains["fqdn"] }}"
      capture:
        - "request header Host len 250"
        - "cookie SERVERID171248 len 32"
