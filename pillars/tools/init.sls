# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Tools stuff pillar

# wanted everywhere
{% set mandatories  = [ 'haproxy' ] %}

# compute a list of pillars we want to include
{%- set enabled = [] %}
{%- for component in mandatories %}
{%-   do enabled.append(slspath + "." + component) %}
{%- endfor %}

# include wanted pillars
include: {{ enabled }}
