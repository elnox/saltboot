# Saltbench

Saltstack bootstrapper

```bash
bash <(curl -s https://gitlab.com/elnox/saltboot/-/raw/main/utils/install) --fqdn host.domain.tld --group <backend | client>
salt-call --local state.apply
```

# BENCHMARK: http://10.100.100.142:80 - 2023/06/04 - 02:08

**command: h1load -c 160 -d 120s**

```bash
PING 10.100.100.142 (10.100.100.142) 56(84) bytes of data.
64 bytes from 10.100.100.142: icmp_seq=1 ttl=60 time=0.143 ms
64 bytes from 10.100.100.142: icmp_seq=2 ttl=60 time=0.111 ms
64 bytes from 10.100.100.142: icmp_seq=3 ttl=60 time=0.082 ms

--- 10.100.100.142 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 56ms
rtt min/avg/max/mdev = 0.082/0.112/0.143/0.024 ms

Start: 2023-04-06T02:08:33+0200
HOST: attack                      Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.200.100.254             0.0%    10    0.3   0.2   0.2   0.3   0.1
  2.|-- 10.50.20.35                0.0%    10    0.2   0.2   0.2   0.3   0.0
  3.|-- 10.50.20.34                0.0%    10    0.6   0.7   0.4   1.0   0.1
  4.|-- 10.50.20.123               0.0%    10    0.2   0.2   0.2   0.3   0.0
  5.|-- 10.100.100.142             0.0%    10    0.1   0.1   0.1   0.2   0.0
```

## run file size: 0 bit

```bash
curl -I http://10.100.100.142:80/?s=0

HTTP/1.1 200 
content-length: 0
x-req: size=369, time=0 ms
x-rsp: id=dummy, code=200, cache=1, size=0, time=0 ms (0 real)

```

**average**

```bash
rate:        352073 r/sec
bits:       628.098 M/sec
ttfb:          .443  msec
ttlb:          .443  msec
```

**percentile**

```bash
#pctl   tail   invtail   ttfbcnt ttfb(ms)   ttlbcnt ttlb(ms)
10      90     1         3248529     0.33   3248529     0.33
50      50     2        16413594    0.433  16413594    0.433
75      25     4        24599821    0.506  24599821    0.506
90      10     10       29544655    0.583  29544655    0.583
95      5      20       31218087    0.634  31218087    0.634
99      1      100      32539571    0.739  32539571    0.739
99.99   0.01   10000    32866244     5.46  32866244     5.46
99.9999 0.0001 1000000  32869503   18.784  32869503   18.784
```

**graphs**

![latency](reports/png/10199108129-http-0-pctl.png "latency") ![load](reports/png/10199108129-http-0-load.png "load")

**raw**

- [10199108129-http-0-pctl](reports/raw/10199108129-http-0.dat-pctl)
- [10199108129-http-0-load](reports/raw/10199108129-http-0.dat-load)

