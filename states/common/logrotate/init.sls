# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Logrotate management states

{%- from "common/logrotate/map.jinja" import logrotate with context %}

logrotate_pkg:
  pkg.installed:
    - name: {{ logrotate.pkg }}
    - failhard: True

logrotate_cnf:
  file.managed:
    - name: {{ logrotate.config_path }}
    - source: {{ logrotate.config_src }}
    - template: jinja
    - user: {{ logrotate.user }}
    - group: {{ logrotate.group }}
    - mode: {{ logrotate.mode }}

logrotate_dir:
  file.directory:
    - name: {{ logrotate.include_dir }}
    - user: {{ logrotate.user }}
    - group: {{ logrotate.group }}
    - mode: 755
    - makedirs: True

{%- for job, dataset in logrotate.jobs | dictsort %}
{%-   if dataset.present is sameas true %}
logrotate_{{ job }}:
  file.managed:
    - name: {{ logrotate.include_dir }}/{{ job.split("/")[-1] }}
    - source: {{ logrotate.job_src }}
    - template: jinja
    - user: {{ logrotate.user }}
    - group: {{ logrotate.group }}
    - mode: {{ logrotate.mode }}
    - context:
      dataset: {{ dataset | json }}
{%-   else %}
logrotate_{{ job }}:
  file.absent:
    - name: {{ logrotate.include_dir }}/{{ job.split("/")[-1] }}
{%-   endif %}
{%- endfor %}
