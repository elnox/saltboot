# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Locale management states

{%- from "common/locale/map.jinja" import locale with context %}

locale_pkg:
  pkg.installed:
    - name: {{ locale.pkg }}
    - failhard: True

{%- for l in locale.present %}
locale_present_{{ l | replace('.', '_') | replace(' ', '_') }}:
  locale.present:
    - name: {{ l }}
{%- endfor %}

{%- if locale.default is defined %}
locale_default:
  locale.system:
    - name: {{ locale.default.name }}
    - require:
      - locale: locale_present_{{ locale.default.require | replace('.', '_') | replace(' ', '_') }}
{%- endif %}

{%- if locale.conf is defined %}
locale_env:
  file.managed:
    - name: {{ locale.config_path }}
    - user: {{ locale.user }}
    - group: {{ locale.group }}
    - mode: {{ locale.mode }}
    - contents_pillar: locale:conf
{%- endif %}
