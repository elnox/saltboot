# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Common meta state

include:
  - {{ slspath }}.apt
  - {{ slspath }}.bash
  - {{ slspath }}.cron
  - {{ slspath }}.debian
  - {{ slspath }}.hostname
  - {{ slspath }}.locale
  - {{ slspath }}.logrotate
  - {{ slspath }}.repos
  - {{ slspath }}.sysctl
  - {{ slspath }}.syslog
  - {{ slspath }}.timezone
  - {{ slspath }}.vim
