# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Apt meta state

{%- from "common/apt/map.jinja" import apt with context %}

apt_pkgs:
  pkg.installed:
    - pkgs: {{ apt.pkgs | json }}
    - failhard: True

apt_cnf:
  file.managed:
    - name: {{ apt.conf_path }}
    - mode: '0644'
    - user: root
    - group: root
    {%- if apt.remove_apt_conf %}
    - contents: ''
    - contents_newline: False
    {%- else %}
    - replace: False
    {%- endif %}

apt_cnf_dir:
  file.directory:
    - name: {{ apt.conf_dir }}
    - mode: 755
    - user: root
    - group: root
    - clean: {{ apt.conf_dir_clean }}

{%- for file, contents in apt.confd | dictsort %}
apt_cnf_{{ file }}:
  file.managed:
    - name: {{ apt.conf_dir }}/{{ file }}
    - source: salt://common/files/apt.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        data: {{ contents }}
{%- endfor %}

apt_listchanges_cnf:
  file.managed:
    - name: {{ apt.listchanges_path }}
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - source: salt://common/files/listchanges.jinja

apt_preferences_cnf:
  file.managed:
    - name: {{ apt.preferences_path }}
    - mode: '0644'
    - user: root
    - group: root
    {%- if apt.remove_preferences %}
    - contents: ''
    - contents_newline: False
    {%- else %}
    - replace: False
    {% endif %}

apt_preferences_dir:
  file.directory:
    - name: {{ apt.preferences_dir }}
    - mode: '0755'
    - user: root
    - group: root
    - clean: {{ apt.preferences_clean }}

{%- for file, args in apt.preferences | dictsort %}
{%-   set p_package = args.package if args.package is defined else '*' %}
apt_preferences_{{ file }}:
  file.managed:
    - name: {{ apt.preferences_dir }}/{{ file }}
    - mode: '0644'
    - user: root
    - group: root
    - contents:
      - "{{ 'Package: ' ~ p_package }}"
      - "{{ 'Pin: ' ~ args.pin }}"
      - "{{ 'Pin-Priority: ' ~ args.priority }}"
      {%- if 'explanation' in args %}
      {%-   for explanation in args.explanation %}
      - "{{ 'Explanation: ' ~ explanation }}"
      {%-   endfor %}
      {%- endif %}
{%- endfor %}

apt_unattended_cnf:
  file.managed:
    - name: {{ apt.conf_dir }}/{{ apt.unattended_config }}
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - source: salt://common/files/unattended.jinja

apt_periodic_cnf:
  file.managed:
    - name: {{ apt.conf_dir }}/{{ apt.periodic_config }}
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - source: salt://common/files/periodic.jinja

apt_sources_list:
  file.managed:
    - name: {{ apt.sources_list_path }}
    - mode: '0644'
    - user: root
    - group: root
    {%- if apt.remove_sources_list %}
    - contents: ''
    - contents_newline: False
    {%- else %}
    - replace: False
    {%- endif %}

apt_sources_list_dir:
  file.directory:
    - name: {{ apt.sources_list_dir }}
    - mode: '0755'
    - user: root
    - group: root
    - clean: {{ apt.sources_list_clean }}

{%- for repo, args in apt.get('repositories', {}) | dictsort %}
{%-   set r_opts = '' %}
{%-   set r_arch = 'arch=' ~ args.arch | join(',') if args.arch is defined else '' %}
{%-   if args.opts is defined %}
{%-     if args.opts is string %}
{%-       set r_opts = args.opts %}
{%-     else %}
{%-       set r_opts_list = [] %}
{%-       for k, v in args.opts | dictsort %}
{%-         do r_opts_list.append(k ~ '=' ~ v) %}
{%-       endfor %}
{%-       set r_opts =  r_opts_list | join(' ') %}
{%-     endif %}
{%-   endif %}
{%-   if r_arch != '' or r_opts != '' %}
{%-     set r_options = '[' ~ r_arch ~ ' ' ~ r_opts ~ ' ]' %}
{%-   else %}
{%-     set r_options = '' %}
{%-   endif %}
{%-   set r_url = args.url or apt.default_url %}
{%-   set r_distro = args.distro or 'stable' %}
{%-   set r_comps = args.comps | default(['main']) | join(' ') %}
{%-   set r_keyserver = args.keyserver if args.keyserver is defined else apt.default_keyserver %}
{%-   for type in args.type | default(['binary']) %}
{%-     set r_type = 'deb-src' if type == 'source' else 'deb' %}
{{ r_type }}_{{ repo }}:
  pkgrepo.managed:
    - name: {{ r_type }} {{ r_options }} {{ r_url }} {{ r_distro }} {{ r_comps }}
    - file: {{ apt.sources_list_dir }}/{{ repo }}-{{ type }}.list
    {%- if args.key_url is defined %}
    - key_url: {{ args.key_url }}
    {%- elif args.keyid is defined %}
    - keyid: {{ args.keyid }}
    - keyserver: {{ r_keyserver }}
    {%- endif %}
{%-   endfor %}
{%- endfor %}

{%- if not salt['grains.get']('bootstrap') %}
apt_update:
  cmd.wait:
    - name: 'apt-get update'

apt_upgrade:
  cmd.wait:
    - name: 'echo y | apt-get upgrade'
    - watch:
      - file: /etc/apt/sources.list
      - file: /etc/apt/sources.list.d
  grains.present:
    - name: bootstrap
    - force: true
    - value: true
{%- endif %}
