# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Hostname meta state

{%- from "common/hostname/map.jinja" import hostname with context %}

hostname_file:
  file.managed:
    - name: {{ hostname.path }}
    - user: root
    - group: root
    - contents_pillar: hostname:fqdn

hostname_set_fqdn:
  cmd.run:
    {%- if grains["init"] == "systemd" %}
    - name: hostnamectl set-hostname {{ hostname.fqdn }}
    {%- else %}
    - name: hostname {{ hostname.fqdn }}
    {%- endif %}
    - unless: test "{{ hostname.fqdn }}" = "$(hostname)"
    - onchanges:
      - file: hostname_file

{%- if hostname.manage_hosts is sameas true %}
hostname_ips_present:
  host.present:
    - ip: {{ hostname.ips }}
    - names:
      - {{ hostname.fqdn }}
      - {{ hostname.name }}
{%- endif %}

hostname_grain_host:
  grains.present:
    - name: host
    - value: {{ hostname.name }}
    - onchanges:
      - file: hostname_file

hostname_grain_fqdn:
  grains.present:
    - name: fqdn
    - value: {{ hostname.fqdn }}
    - onchanges:
      - file: hostname_file

hostname_refresh:
  cmd.run:
    - name: "salt-call saltutil.refresh_grains"
    - onchanges:
      - file: hostname_file
