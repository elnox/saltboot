# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Timezone management states

{%- from "common/timezone/map.jinja" import timezone with context %}

timezone_pkg:
  pkg.installed:
    - name: {{ timezone.pkg }}
    - failhard: True

timezone_setting:
  timezone.system:
    - name: {{ timezone.name }}
    - utc: {{ timezone.utc }}

timezone_symlink:
  file.symlink:
    - name: {{ timezone.localtime }}
    - target: {{ timezone.zoneinfo }}{{ timezone.name }}
    - force: true
    - require:
      - pkg: {{ timezone.pkg }}
