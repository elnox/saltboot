# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Repos meta state

{%- from "common/repos/map.jinja" import repos with context %}

repos_pkg:
  pkg.installed:
    - name: {{ repos.pkg }}
    - failhard: True

{%- for dir in repos.remove %}
repos_remove_{{ dir }}:
  file.absent:
    - name: {{ dir }}
{%- endfor %}

{%- for name, options in repos.deploy | dictsort %}
target_{{ name }}:
    file.directory:
    - name: {{ options.target }}
    {%- if options.user is defined %}
    - user: {{ options.user }}
    - group: {{ options.user }}
    {%- else %}
    - user: {{ repos.user }}
    - group: {{ repos.group }}
    {%- endif %}
    - makedirs: true

{%- if options.detached is sameas true %}
repos_{{ name }}:
  git.detached:
    - name: {{ options.url }}
    - target: {{ options.target }}
    - force_clone: {{ repos.force }}
    - force_fetch: {{ repos.force }}
    {%- if options.rev is defined %}
    - rev: {{ options.rev }}
    {%- endif %}
    {%- if options.nosync is defined and options.nosync is sameas true %}
    - unless: test -d {{ options.target }}
    {%- endif %}

{%- else %}
repos_{{ name }}:
  git.latest:
    - name: {{ options.url }}
    - target: {{ options.target }}
    - force_reset: {{ options.reset }}
    - force_clone: {{ repos.force }}
    - force_fetch: {{ repos.force }}
    {%- if options.rev is defined %}
    - rev: {{ options.rev }}
    {%- endif %}
    {%- if options.branch is defined %}
    - branch: {{ options.branch }}
    {%- endif %}
    {%- if options.remote is defined %}
    - remote: {{ options.remote }}
    {%- endif %}
    {%- if options.identity is defined %}
    - identity: {{ options.identity }}
    - onlyif: test -f {{ options.identity }}
    {%- endif %}
    {%- if options.nosync is defined and options.nosync is sameas true %}
    - unless: test -d {{ options.target }}
    {%- endif %}
  {%- if options.user is defined %}
  file.directory:
    - name: {{ options.target }}
    - user: {{ options.user }}
    - group: {{ options.user }}
    - recurse:
        - user
        - group
  {%- endif %}
{%- endif %}
{%- endfor %}
