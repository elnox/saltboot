# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables services state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable and iptables.services is defined %}

#
# Available protocols
#
{%- set ipversions = [ 'ipv4' ] %}
{%- if iptables.get('ipv6', False) %}
{%-   do ipversions.append('ipv6') %}
{%- endif %}

# Loop over IP protocols
{%- for ipver in ipversions %}

#
# Generate ipsets for services
#
{%- for service, details in iptables.services.get(ipver, {}) | dictsort %}
{%-   set block_nomatch = details.get('block_nomatch', False) %}
{%-   set interfaces = details.get('interfaces','') %}
{%-   set protos = details.get('protos',['tcp']) %}
{%-   if details.get('comment', False) %}
{%-     set comment = '- comment: ' + details.get('comment') %}
{%-   else %}
{%-     set comment = '' %}
{%-   endif %}

# Allow rules for ips/subnets
{%-   for ip in details.get('ips_allow', ['0.0.0.0/0']) %}

# rules applied on any interfaces
{%-     if interfaces == '' %}
{%-       for proto in protos %}
iptables-{{ service }}-allow-{{ ip }}-{{ proto }}-{{ ipver }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - source: {{ ip }}
    {%- if service.split(',') | length > 1 %}
    - dports: {{ service }}
    {%- elif proto != 'icmp' %}
    - dport: {{ service }}
    {%- endif %}
    - proto: {{ proto }}
    {%- if details.get('icmp-type') %}
    - icmp-type: {{ details.get('icmp-type') }}
    {%- endif %}
    - family: {{ ipver }}
    {{ comment }}
    - save: True
{%-       endfor %}

# rules applied on defined interfaces
{%-     else %}
{%-       for iface in interfaces %}
{%-         for proto in protos %}
iptables-{{ service }}-allow-{{ ip }}-{{ proto }}-{{ iface }}-{{ ipver }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - in-interface: {{ interface }}
    - source: {{ ip }}
    {%- if proto != 'icmp' %}
    - dport: {{ service }}
    {%- endif %}
    - proto: {{ proto }}
    {%- if details.get('icmp-type') %}
    - icmp-type: {{ details.get('icmp-type') }}
    {%- endif %}
    - family: {{ ipver }}
    {{ comment }}
    - save: True
{%-         endfor %}
{%-       endfor %}
{%-     endif %}
{%-   endfor %}

{%- endfor %} # end services loop
{%- endfor %} # end ipversions loop
{%- endif %}
