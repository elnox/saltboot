# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables policies state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable %}

#
# Available protocols
#
{%- set ipversions = [ 'ipv4' ] %}
{%- if iptables.get('ipv6', False) %}
{%-   do ipversions.append('ipv6') %}
{%- endif %}

# Loop over IP protocols
{%- for ipver in ipversions %}

#
# Strict mode
#
{%- if iptables.strict %}

# allow loopback access
{%-   if ipver == 'ipv4' %}
iptables-allow-loopback-{{ ipver }}:
  iptables.insert:
    - position: 1
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - in-interface: lo
    - comment: "Allow loopback traffic"
    - save: True
{%-   endif %}

# allow established
iptables-allow-established-{{ ipver }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: conntrack
    - ctstate: 'RELATED,ESTABLISHED'
    - family: {{ ipver }}
    - save: True

# generate rules for whitelisted networks
{%-   if iptables.whitelist is defined %}
{%-     for network in iptables.whitelist.get(ipver, []) %}
iptables-whitelist-{{ network }}-{{ ipver }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - source: {{ network }}
    - family: {{ ipver }}
    - comment: "Whitelisted"
    - save: True
{%-     endfor %}
{%-   endif %}

# deny everything else unless defined
iptables-enable-drop-policy-{{ ipver }}:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: DROP
    - family: {{ ipver }}

#
# Relax mode
#
{%- else %}

# generate rules for blacklisted networks
{%-   if iptables.blacklist is defined %}
{%-     for network in iptables.blacklist.get(ipver, []) %}
iptables-blacklist-{{ network }}-{{ ipver }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: DROP
    - source: {{ network }}
    - family: {{ ipver }}
    - comment: "Blacklisted"
    - save: True
{%-     endfor %}
{%-   endif %}

# accept anything else
iptables-enable-accept-policy-{{ ipver }}:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: ACCEPT
    - family: {{ ipver }}
    - save: True

{%- endif %}
{%- endfor %}

#
# Generate rules for NAT
#
{%- for iface, details in iptables.get('nat', {}) | dictsort %}
{%-   for src, dest in details.get('rules', {}) | dictsort %}
{%-     for ip in dest %}
iptables-{{ iface }}-allow-{{ src }}-{{ ip }}:
  iptables.append:
    - table: nat
    - chain: POSTROUTING
    - jump: MASQUERADE
    - out-interface: {{ iface }}
    - source: {{ src }}
    - destination: {{ ip }}
    - save: True
{%-     endfor %}
{%-   endfor %}
{%- endfor %}

#
# Firewall is disabled
#
{%- else %}
iptables-disabled:
  test.show_notification:
    - name: Firewall is disabled by default
    - text: firewall:enabled is False
{%- endif %}
