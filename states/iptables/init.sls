# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables meta state

{% from "iptables/map.jinja" import iptables with context %}

#
# iptables enabler
#
{% if iptables.enable is sameas true %}

#
# legacy management
#
{% if iptables.legacy is sameas true %}
include:
  - {{ slspath }}.legacy
  - {{ slspath }}.arptables

#
# full management
#
{% else %}
include:
  - {{ slspath }}.install
  - {{ slspath }}.policies
  - {{ slspath }}.services
  - {{ slspath }}.tables
  - {{ slspath }}.restore
  - {{ slspath }}.arptables
{% endif %}

#
# iptables management is disabled
#
{% else %}
iptables-disabled:
  test.show_notification:
    - name: iptables management is disabled
    - text: iptables:enable is False

{% endif %}
