# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Arptables state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable %}

# Add chain rules for wanted interface
{%- for iface, rules in iptables.arptables | dictsort %}
arptables-rules-{{ iface }}:
  file.managed:
    - name: /etc/network/if-up.d/arptable-{{ iface }}
    - source: {{ iptables.arptable_src }}
    - template: jinja
    - user: root
    - group: root
    - mode: 755
    - context:
        iface: {{ iface }}
        rules: {{ rules }}

arptables-apply-{{ iface }}:
  environ.setenv:
    - name: IFACE
    - value: {{ iface }}
    - onchanges:
      - file: arptables-rules-{{ iface }}
  cmd.run:
    - name: /etc/network/if-up.d/arptable-{{ iface }}
    - onchanges:
      - file: arptables-rules-{{ iface }}
{%- endfor %}

{%- endif %}
