# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables tables state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable %}

#
# Iterate on supported tables
#
{%- for table in ['filter','mangle'] %}

#
# Ensure custom chains exist
#
{%-   for chain, data in iptables.get(table) | dictsort %}
{%-     if chain not in ['INPUT','OUTPUT','FORWARD','PREROUTING','POSTROUTING'] %}
iptables-chain-present-{{ table }}-{{ chain | lower }}:
  iptables.chain_present:
    - table: {{ table }}
    - name: {{ chain }}
{%-     endif %}
{%-   endfor %}

#
# Add chain rules
#
{%-   for chain, data in iptables.get(table) | dictsort %}
{%-     set rules = data.rules | default({}) %}
{%-     for rule, values in rules | dictsort %}
iptables-rule-{{ table }}-{{ chain | lower }}-{{ rule | lower }}:
  {%- if values['position'] is defined %}
  iptables.insert:
    - position: {{ values['position'] }}
  {%- else %}
  iptables.append:
  {%- endif %}
    - table: {{ table }}
    - chain: {{ chain }}
    {%- for k, v in values | dictsort %}
    - {{ k }}: '{{ v }}'
    {%- endfor %}
    - save: true
    {%- if chain not in ['INPUT','OUTPUT','FORWARD','PREROUTING','POSTROUTING'] %}
    - require:
      - iptables: iptables-chain-present-{{ table }}-{{ chain | lower }}
    {%- endif %}
{%-     endfor %}

#
# Set chain policies
#
{%-      if chain in ['INPUT','OUTPUT','FORWARD','PREROUTING','POSTROUTING'] %}
{%-       if iptables.strict and chain == 'INPUT' and table == 'filter' %}
{%-         set policy = 'DROP' %}
{%-       else %}
{%-         set policy = data['policy'] | default('ACCEPT') %}
{%-       endif %}
iptables-policy-{{ table }}-{{ chain | lower }}-{{ policy | lower }}:
  iptables.set_policy:
    - table: {{ table }}
    - chain: {{ chain }}
    - policy: {{ policy }}
    - save: true
{%-      endif %}
{%-   endfor %}
{%- endfor %}
{%- endif %}
