# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables install state

{% from "iptables/map.jinja" import iptables with context %}

#
# install required packages
#
iptables-pkgs:
  pkg.installed:
    - pkgs: {{ iptables.pkgs | json }}
    - failhard: True
