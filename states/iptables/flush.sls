# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables flush and restore state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable %}

#
# Available protocols
#
{%- set ipversions = [ 'ipv4' ] %}
{%- if iptables.get('ipv6', False) %}
{%-   do ipversions.append('ipv6') %}
{%- endif %}

# Loop over IP protocols
{%- for ipver in ipversions %}

# Set the policy to allow everything before flushing rules
{%- set chains = ['INPUT', 'FORWARD', 'OUTPUT'] %}
{%- for chain in chains %}
iptables-flush-{{ chain | lower }}-allow-policy-{{ ipver }}:
  iptables.set_policy:
    - table: filter
    - chain: {{ chain }}
    - policy: ACCEPT
    - family: {{ ipver }}
{%- endfor %}

# Flush tables
{%- set tables = ['filter', 'nat', 'mangle'] %}
{%- set flush_chains = iptables.get('flush_chains', {}) %}
{%- for table in tables %}
{%-   if flush_chains.get(table) is iterable %}
{%-     for chain in flush_chains.get(table) %}
iptables-flush-{{ table }}-{{ chain | lower }}-{{ ipver }}:
  iptables.flush:
    - table: {{ table }}
    - family: {{ ipver }}
    - chain: {{ chain }}
{%-     endfor %}
{%-   else %}
iptables-flush-{{ table }}-{{ ipver }}:
  iptables.flush:
    - table: {{ table }}
    - family: {{ ipver }}
{%-   endif %}
{%- endfor %}

{%- endfor %}
{%- endif %}
