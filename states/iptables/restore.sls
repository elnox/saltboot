# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables flush and restore state

{% from "iptables/map.jinja" import iptables with context %}

#
# Firewall is enabled
#
{%- if iptables.enable %}

#
# Available protocols
#
{%- set ipversions = [ 'ipv4' ] %}
{%- if iptables.get('ipv6', False) %}
{%-   do ipversions.append('ipv6') %}
{%- endif %}

# Loop over IP protocols
{%- for ipver in ipversions %}

# Set the policy to allow everything before flushing rules
{%- set chains = ['INPUT', 'FORWARD', 'OUTPUT'] %}
{%- for chain in chains %}
iptables-restore-{{ chain | lower }}-allow-policy-{{ ipver }}:
  iptables.set_policy:
    - table: filter
    - chain: {{ chain }}
    - policy: ACCEPT
    - family: {{ ipver }}
    - onchanges_any:
      - sls: {{ slspath }}.policies
      - sls: {{ slspath }}.services
      - sls: {{ slspath }}.tables
{%- endfor %}

# Flush tables
{%- set tables = ['filter', 'nat', 'mangle'] %}
{%- set flush_chains = iptables.get('flush_chains', {}) %}
{%- for table in tables %}
{%-   if flush_chains.get(table) is iterable %}
{%-     for chain in flush_chains.get(table) %}
iptables-restore-{{ table }}-{{ chain | lower }}-{{ ipver }}:
  iptables.flush:
    - table: {{ table }}
    - family: {{ ipver }}
    - chain: {{ chain }}
    - onchanges_any:
      - sls: {{ slspath }}.policies
      - sls: {{ slspath }}.services
      - sls: {{ slspath }}.tables
{%-     endfor %}
{%-   else %}
iptables-restore-{{ table }}-{{ ipver }}:
  iptables.flush:
    - table: {{ table }}
    - family: {{ ipver }}
    - onchanges_any:
      - sls: {{ slspath }}.policies
      - sls: {{ slspath }}.services
      - sls: {{ slspath }}.tables
{%-   endif %}
{%- endfor %}

# Restore firewall rules
iptables-restore-{{ ipver }}:
  cmd.run:
    {% if ipver == 'ipv6' %}
    - name: 'ip6tables-restore < /etc/iptables/rules.v6'
    {% else %}
    - name: 'iptables-restore < /etc/iptables/rules.v4'
    {% endif %}
    - onchanges_any:
      - sls: {{ slspath }}.policies
      - sls: {{ slspath }}.services
      - sls: {{ slspath }}.tables

{%- endfor %}
{%- endif %}
