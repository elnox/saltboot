# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iptables legacy state

{% from "iptables/map.jinja" import iptables with context %}

#
# legacy management
#
{% if iptables.legacy is sameas true %}

iptables-pkgs:
  pkg.installed:
    - name: iptables
    - failhard: True

iptables-legacy:
  alternatives.set:
    - name: iptables
    - path: /usr/sbin/iptables-legacy

{% for ipver in [ 'v4', 'v6' ] %}
iptables-rules-{{ ipver }}:
  file.managed:
      - name: /etc/iptables/rules.{{ ipver }}
      - user: root
      - group: root
      - mode: 644
      - source: salt://iptables/files/rules{{ ipver }}.jinja
      - template: jinja
      - makedirs: true

iptables-systemd-{{ ipver }}:
  file.managed:
    {% if ipver == 'v6' %}
    - name: {{ iptables.systemd_lib }}/ip6tables.service
    {% else %}
    - name: {{ iptables.systemd_lib }}/iptables.service
    {% endif %}
    - source: {{ iptables.systemd }}
    - template: jinja
    - context:
        ipver: {{ ipver }}

iptables-service-{{ ipver }}:
  service.enabled:
    {% if ipver == 'v6' %}
    - name: ip6tables.service
    {% else %}
    - name: iptables.service
    {% endif %}

# prevents unwanted iptables-restore
{%   if iptables.restore is sameas true %}
iptables-restore-{{ ipver }}:
  service.running:
    {% if ipver == 'v6' %}
    - name: ip6tables.service
    {% else %}
    - name: iptables.service
    {% endif %}
  cmd.run:
    {% if ipver == 'v6' %}
    - name: 'ip6tables-restore < /etc/iptables/rules.v6'
    {% else %}
    - name: 'iptables-restore < /etc/iptables/rules.v4'
    {% endif %}
    - onchanges_any:
      - file: iptables-rules-{{ ipver }}
{%   endif %}

{% endfor %}

iptables-banner:
  file.append:
    - name: /root/.systemctl_status
    - text:
      - "systemctl status -n 0 iptables.service | grep Active | cut -d ':' -f 2,3,4 | xargs echo iptables.service"
      - "systemctl status -n 0 ip6tables.service | grep Active | cut -d ':' -f 2,3,4 | xargs echo ip6tables.service"
    - onlyif: test -f /root/.systemctl_status

{% endif %}
