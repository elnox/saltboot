# -*- coding: utf-8 -*-
# vim: ft=sls
#
# States top file
#
# default salt environment

{{saltenv}}:

  # these states will be applied to all hosts
  '*':
    - common
    - iptables
    - benchmark
