# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Dpbench management states

{%- from "benchmark/dpbench/map.jinja" import dpbench with context %}

# Dpbench enabler
{% if dpbench.enable is sameas true %}

dpbench_pkg:
  pkg.installed:
    - pkgs: {{ dpbench.pkgs | json }}
    - failhard: True

dpbench_dir:
  file.directory:
    - name: {{ dpbench.target }}
    - user: {{ dpbench.user }}
    - group: {{ dpbench.group }}
    - makedirs: true

dpbench_clone:
  git.latest:
    - name: {{ dpbench.url }}
    - target: {{ dpbench.target }}
    - force_reset: {{ dpbench.reset }}
    - rev: {{ dpbench.rev }}
    - branch: {{ dpbench.branch }}
    - submodules: true

dpbench_build:
  cmd.run:
    - name: {{ dpbench.target }}/tools/build-all.sh > /dev/null 2>&1
    - unless:
      - fun: file.directory_exists
        path: {{ dpbench.target }}/bin/

{%- for bin in dpbench.binaries %}
{%-   set filename = salt['file.basename'](bin) %}
dpbench_symlink_{{ filename }}:
  file.symlink:
    - name: /usr/local/bin/{{ filename }}
    - target: {{ dpbench.target }}/bin/{{ bin }}
    - force: true
    - onlyif:
      - fun: file.file_exists
        path: {{ dpbench.target }}/bin/{{ bin }}
{%- endfor %}

{%- if dpbench.hserver is sameas true %}
dpbench_hserver_script:
  file.managed:
    - name: /usr/local/bin/hserver
    - user: {{ dpbench.user }}
    - group: {{ dpbench.group }}
    - mode: 744
    - template: jinja
    - source: {{ dpbench.hserver_script }}

dpbench_hserver_run:
  cmd.run:
    - name: '/usr/local/bin/hserver {{ dpbench.hserver_port }}'
    - unless: pgrep httpterm >/dev/null
{%- endif %}

# Dpbench management is disabled
{%- else %}
dpbench_disabled:
  test.show_notification:
    - name: dpbench management is disabled
    - text: dpbench:enable is False
{% endif %}
