# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Iperf management states

{%- from "benchmark/iperf/map.jinja" import iperf with context %}

# Iperf enabler
{% if iperf.enable is sameas true %}

iperf_pkg:
  pkg.installed:
    - name: {{ iperf.pkg }}
    - failhard: True

iperf_systemd:
  file.managed:
    - name: {{ iperf.systemd_path }}/{{ iperf.service }}@.service
    - user: {{ iperf.user }}
    - group: {{ iperf.group }}
    - mode: 644
    - template: jinja
    - source: {{ iperf.systemd_src }}

iperf_service:
  {% if iperf.service_enable is sameas true %}
  service.running:
    - name: {{ iperf.service }}@{{ iperf.port }}
    - enable: {{ iperf.service_enable }}
    - reload: False
    - require:
      - pkg: iperf_pkg
      - file: iperf_systemd
    {% if iperf.watch is sameas true %}
    - watch:
      - pkg: iperf_pkg
      - file: iperf_systemd
    {% endif %}
  {% else %}
  service.dead:
    - name: {{ iperf.service }}@{{ iperf.port }}
    - enable: False
  {% endif %}

iperf_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 iperf3@5201 | grep Active | cut -d ':' -f 2,3,4 | xargs echo iperf3@5201"

# Iperf management is disabled
{%- else %}
iperf_disabled:
  test.show_notification:
    - name: iperf management is disabled
    - text: iperf:enable is False
{% endif %}
