# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Benchmark meta state

include:
  - {{ slspath }}.bombardier
  - {{ slspath }}.dpbench
  - {{ slspath }}.iperf
