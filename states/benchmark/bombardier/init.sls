# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Bombardier state

{% from "benchmark/bombardier/map.jinja" import bombardier with context %}

# Bombardier enabler
{% if bombardier.enable is sameas true %}

bombardier_get:
  file.managed:
    - name: /usr/local/bin/bombardier
    - source: {{ bombardier.url }}/v{{ bombardier.version }}/bombardier-linux-amd64
    - skip_verify: true
    - user: root
    - group: root
    - mode: 755
    - unless:
      - fun: file.file_exists
        path: /usr/local/bin/bombardier

# Bombardier management is disabled
{%- else %}
bombardier_disabled:
  test.show_notification:
    - name: bombardier management is disabled
    - text: bombardier:enable is False
{% endif %}
