# -*- coding: utf-8 -*-
# vim: ft=sls
#
# Haproxy meta state

{% from "tools/haproxy/map.jinja" import haproxy with context %}

# Haproxy enabler
{% if haproxy.enable is sameas true %}

haproxy_pkgs:
  pkg.installed:
    - pkgs: {{ haproxy.pkgs | json }}
    - failhard: True

haproxy_chroot:
  file.directory:
    - name: {{ haproxy.chroot_path }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}

haproxy_cnf:
 file.managed:
    - name: {{ haproxy.config_path }}
    - source: {{ haproxy.config_src }}
    - template: jinja
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}
    - mode: 644
    - listen_in:
      - service: haproxy_service

{% for name, path in haproxy.scripts | dictsort %}
haproxy_script_{{ name }}:
 file.managed:
    - name: {{ path }}/{{ name }}
    - source: {{ haproxy.alt_src_path }}/{{ name }}
    - user: {{ haproxy.user }}
    - group: {{ haproxy.group }}
    - mode: 644
    - listen_in:
      - service: haproxy_service
{% endfor %}

haproxy_service:
  {% if haproxy.service_enable is sameas true %}
  service.running:
    - name: {{ haproxy.service }}
    - enable: {{ haproxy.service_enable }}
    - reload: True
    - require:
      - pkg: haproxy_pkgs
    {% if haproxy.watch is sameas true %}
    - watch:
      - pkg: haproxy_pkgs
      - file: haproxy_cnf
    {% endif %}
  {% else %}
  service.dead:
    - name: {{ haproxy.service }}
    - enable: False
  {% endif %}

haproxy_banner:
  file.append:
    - name: /var/spool/.systemctl_status
    - text:
      - "systemctl status -n 0 haproxy | grep Active | cut -d ':' -f 2,3,4 | xargs echo haproxy"

# Haproxy management is disabled
{%- else %}
haproxy_disabled:
  test.show_notification:
    - name: haproxy management is disabled
    - text: haproxy:enable is False
{% endif %}
