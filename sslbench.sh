#!/bin/bash



echo -en "TLS1.1 ECDHE-RSA-AES128-SHA\t\t"
[ -f tls.txt ] && rm -f tls.txt
run=0
while (( run != $iterate )); do
	((run += 1))
	#echo $run
	curl --ciphers ECDHE-RSA-AES128-SHA --tlsv1.1 -w @appconnect.txt https://iplb.drakonix.net/0k -s -o /dev/null >> tls.txt
	sleep $tempo
done

count=0
x=0
for i in `cat tls.txt | sed "s/time_appconnect: //" | sed "s/s//" | sed "s/,/./"`; do
        sum="$count + $i"
        count=`bc <<< $sum`
        #echo $count
		(( x += 1 ))
done
bc <<< "scale=7; $count/$x"

echo -en "TLS1.2 ECDHE-RSA-AES128-SHA\t\t"
run=0
while (( run != $iterate )); do
	((run += 1))
	#echo $run
	curl --ciphers ECDHE-RSA-AES128-SHA --tlsv1.2 -w @appconnect.txt https://iplb.drakonix.net/0k -s -o /dev/null >> tls.txt
	sleep $tempo
done

count=0
x=0
for i in `cat tls.txt | sed "s/time_appconnect: //" | sed "s/s//" | sed "s/,/./"`; do
        sum="$count + $i"
        count=`bc <<< $sum`
        #echo $count
		(( x += 1 ))
done
bc <<< "scale=7; $count/$x"

echo -en "TLS1.2 DHE-RSA-AES256-GCM-SHA384\t"
[ -f tls.txt ] && rm -f tls.txt
run=0
while (( run != $iterate )); do
	((run += 1))
	#echo $run
	curl --ciphers DHE-RSA-AES256-GCM-SHA384 --tlsv1.2 -w @appconnect.txt https://iplb.drakonix.net/0k -s -o /dev/null >> tls.txt
	sleep $tempo
done

count=0
x=0
for i in `cat tls.txt | sed "s/time_appconnect: //" | sed "s/s//" | sed "s/,/./"`; do
        sum="$count + $i"
        count=`bc <<< $sum`
        #echo $count
		(( x += 1 ))
done
bc <<< "scale=7; $count/$x"

echo -en "TLS1.2 ECDHE-RSA-CHACHA20-POLY1305\t"
[ -f tls.txt ] && rm -f tls.txt
run=0
while (( run != $iterate )); do
	((run += 1))
	#echo $run
	curl --ciphers ECDHE-RSA-CHACHA20-POLY1305 --tlsv1.2 -w @appconnect.txt https://iplb.drakonix.net/0k -s -o /dev/null >> tls.txt
	sleep $tempo
done

count=0
x=0
for i in `cat tls.txt | sed "s/time_appconnect: //" | sed "s/s//" | sed "s/,/./"`; do
        sum="$count + $i"
        count=`bc <<< $sum`
        #echo $count
		(( x += 1 ))
done
bc <<< "scale=7; $count/$x"

echo -en "TLS1.3 TLS-AES-256-GCM-SHA384\t\t"
[ -f tls.txt ] && rm -f tls.txt
run=0
while (( run != $iterate )); do
	((run += 1))
	#echo $run
	curl --tlsv1.3 -w @appconnect.txt https://iplb.drakonix.net/0k -s -o /dev/null >> tls.txt
	sleep $tempo
done

count=0
x=0
for i in `cat tls.txt | sed "s/time_appconnect: //" | sed "s/s//" | sed "s/,/./"`; do
        sum="$count + $i"
        count=`bc <<< $sum`
        #echo $count
		(( x += 1 ))
done
bc <<< "scale=7; $count/$x"
